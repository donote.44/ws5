<?php
/**
 * Created by PhpStorm.
 * User: Diiar
 * Date: 24/1/2562
 * Time: 14:51
 */

class ProductController
{
    public function handleRequest(string $action="index", array $params) {
        switch ($action) {
            case "checkout":
                $this->checkout();
                break;
            case "cart":
                $this->cart();
                break;
            case "index":
                $this->index();
                break;
            default:
                break;
        }

    }

    private function checkout(){
        session_start();
        include Router::getSourcePath()."views/checkout.inc.php";
    }

    private function cart(){
        session_start();
        $_SESSION['productList'] = Product::findAll();
        include Router::getSourcePath()."views/cart.inc.php";
    }

    private function index() {

    }
}