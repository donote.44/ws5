<?php
/**
 * Created by PhpStorm.
 * User: PC-8303-01
 * Date: 28/2/2562
 * Time: 11:06
 */

class MemberMapper
{
    private $memberList;
    private const TABLE = "members";

    public function __construct() {

        $con = Db::getInstance();
        $query = "SELECT * FROM ".self::TABLE;
        $stmt = $con->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_CLASS, "Member");
        $stmt->execute();
        $this->memberList  = array();
        while ($member = $stmt->fetch())
        {
            $this->memberList[$member->getId()] = $member;
        }
    }

    public function getAll(): array {
        return $this->memberList;
    }

    public function get(int $id): ?Member {
        return $this->memberList[$id]??null;
    }

    public function update(Member $member) {

        if (isset($this->memberList[$member->getId()])) {
            $query = "UPDATE ".self::TABLE." SET ";
            $memIt = $member->getIterator();
            foreach ($memIt as $prop => $val) {
                $query .= " $prop='$val',";
            }
            $query = substr($query, 0, -1);
            $query .= " WHERE id = ".$member->getId();
            //echo $query;
            $con = Db::getInstance();
            if ($con->exec($query) === true) {
                $this->memberList[$member->getId()] = $member;
                return true;
            }
            return false;
        }
        else {
            throw new Exception("Member doesn't exist");
        }



    }
}